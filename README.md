## Contents

1. [Quick start](#quick-start)


In order to successfully build & run application, you'll need to:

### Quick start

1. `git clone` (https://gitlab.com/NakedSnake2777/kriptomat-task) repository to your local machine
2. Spin up a Laravel server
3. Run application