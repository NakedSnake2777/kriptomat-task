let GECKO_API_URI = "http://localhost:8080";

let UPDATE_INTERVAL = 60 * 1000;

let app = new Vue({
  el: "#app",
  data: {
    coinData: {},
  },
  methods: {

    getCoinData: function(symbol) {
      let self = this;

      axios.get(GECKO_API_URI)
        .then((resp) => {
          this.coinData = resp.data;
          console.log(this.coinData);
        })
        .catch((err) => {
          console.error(err);
        });
    },

    getColor: (num) => {
      return num > 0 ? "color:green;" : "color:red;";
    },
  },

  created: function () {
    this.getCoinData();
  }
});

setInterval(() => {
  app.getCoinData();
}, UPDATE_INTERVAL);
